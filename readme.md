Git Cheatsheet

# Project aanmaken in git
- ga naar gitlab
- maak repo aan
- kopieer HTTPS link
- git bash in htdocs
- git clone [url]

# push naar git
- git add .
- git commit -m "message"
- git push
